
import './style.css';

// Componentes
import Botao from '../Botao';

export default function Cartao(props) {

  let tituloDoCartao = props.text?.titulo ?? 'Título padrão';
  let descricaoDoCartao = props.text?.descricao ?? 'Descrição padrão'

  return (
    <section className="c-cartao">
      <h1 className="c-cartao__titulo">{tituloDoCartao}</h1>
      <div className="c-cartao__corpo">
        <p>{descricaoDoCartao}</p>
        <br />
        <Botao />
      </div>
    </section>
  );
}
