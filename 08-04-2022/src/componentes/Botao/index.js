import { useState } from 'react';

import './style.css';

export default function Botao(props) {

  //   Armazenar     , Modificar              = valor inicial
  let [totalDeCliques, definirTotalDeCliques] = useState(0)

  function contarCliques() {
    
    definirTotalDeCliques(totalDeCliques++)
  }

  /*

    Feito em Javascript...

    let totalDeCliques = 0;

    function contarCliques() {
      totalDeCliques++;
    }
  */

  const textoInicialDoBotao = props.texto ?? 'Texto do botão';

  return (
    <button 
      className="c-botao"
      onClick={contarCliques}
      >
        {totalDeCliques == 0 ? textoInicialDoBotao : `+${totalDeCliques}`}
      </button>
  );
}
