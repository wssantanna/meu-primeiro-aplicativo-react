import React from 'react';
import ReactDOM from 'react-dom';

// Estilos 
import './global.css';

// Páginas / Componentes
import Home from './paginas/Home';

ReactDOM.render(
  <React.StrictMode>
    <Home />
  </React.StrictMode>,
  document.getElementById('root')
);
