
import './style.css';

// Componentes
import Cartao from '../../componentes/Cartao';

// Valores
const conteudoDoCartao = {
  titulo: "Meu primeiro cartão",
  descricao: "A descrição do meu primeiro cartão"
}

function Home() {
  return (
    <div className="l-conteudo">
      <Cartao texto={conteudoDoCartao} />
    </div>
  );
}

export default Home;
