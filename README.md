# Meu primeiro Aplicativo React

## Plano de revisão 

**08 de abril 2022**

- [x] Criar um projeto React.

- [x] Estrutura de pastas.

- [x] Criar um componente.

- [x] Propriedades de um componente.

- [x] Estado de um componente

    - [x] `useState()`

**14 de abril 2022**

- [ ] Estado de um componentes: `Hooks`

    - [ ] `useState()`;
    
    - [ ] `useEffect()`;

    - [ ] `useContext()`;

- [ ] Rotas `React Router DOM`

- [ ] Consumo de uma API

    - [ ] Utilizando [`fetch()`](https://developer.mozilla.org/pt-BR/docs/Web/API/Fetch_API/Using_Fetch)

    - [ ] Utilizando [Axios](https://axios-http.com/ptbr/docs/intro)

## Gravação das aulas

**08 de abril de 2022**

- [x] [Revisão de React parte I](https://drive.google.com/file/d/1KVxrVjdC_6NTBoUoGktYwDFooCmID2aH/view)

**14 de abril de 2022**

- [ ] Revisão de React parte II